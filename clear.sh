#!/bin/bash

for f in $(pwd)/*; do
	fname=$(basename $f)
	if [ ${fname:(-3)} != ".sh" ] && [ $fname != "test-data" ]; then
		rm -r $f
	fi
done
