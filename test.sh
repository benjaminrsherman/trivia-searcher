#!/bin/bash

t=$(echo $1 | cut -f 2 -d '.')

echo $t

feh $1 &

$(pwd)/trivia.sh $t $1
