This is a simple shell script to search for the solutions to questions in online trivia games using Google's search engine.

# Currently supported games:
- HQ
- Cash Show
- BeatTheQ
- SwagIQ

# Dependencies
- convert
- tesseract
- adb
- bash